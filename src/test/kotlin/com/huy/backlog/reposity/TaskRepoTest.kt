package com.huy.backlog.reposity
//
//import com.huy.backlog.model.*
//import org.junit.Assert
//import org.junit.Before
//import org.junit.Test
//import org.junit.runner.RunWith
//import org.mockito.Mockito
//import org.springframework.boot.test.context.SpringBootTest
//import org.springframework.boot.test.mock.mockito.MockBean
//import org.springframework.test.context.junit4.SpringRunner
//import org.springframework.test.web.reactive.server.WebTestClient
//import java.util.*
//
//
//@RunWith(SpringRunner::class)
//@SpringBootTest
//class TaskRepoTest {
//
//    @MockBean
//    lateinit var taskRepo: TaskReposity
//
//    var task1=Task(1, Backlog(1,projectIdentifier = "ABCDE"),"ABCDE-1","","",MStatus.TODO,MPriority.LOW,Date(2020,10,20),Date(2020,10,20),Date(2020,10,20))
//
//    @Test
//    fun findTaskByIdentifer(){
//
//        Mockito.`when`(taskRepo.getSingleTask("ABCDE-1")).thenReturn(Optional.of(task1))
//        var taskTest: Task? = taskRepo.getSingleTask("ABCDE-1").get()
//        Assert.assertEquals(task1,taskTest)
//        Mockito.verify(taskRepo, Mockito.times(1)).getSingleTask("ABCDE-1")
//    }
//
//    @Test
//    fun countTask(){
//        var list: MutableList<Task> = ArrayList<Task>()
//
//        var task2=Task(2, Backlog(1,projectIdentifier = "ABCDE"),"ABCDE-2","","",MStatus.TODO,MPriority.HIGH,Date(2020,10,20),Date(2020,10,20),Date(2020,10,20))
//
//        list.add(task1)
//        list.add(task2)
//
//        Mockito.`when`(taskRepo.getAllTaskByProjectIdentifier("ABCDE-_")).thenReturn(list)
//        Assert.assertEquals(list.size,taskRepo.getAllTaskByProjectIdentifier("ABCDE-_").count())
//
//        Mockito.verify(taskRepo, Mockito.times(1)).getAllTaskByProjectIdentifier("ABCDE-_");
//    }
//
//    @Test
//    fun addTask(){
//        taskRepo.save(task1)
//        Mockito.verify(taskRepo, Mockito.times(1)).save(task1)
//    }
//
//    @Test
//    fun updateTask() {
//        taskRepo.updateTask("updated", Date(2020, 10, 20), MPriority.HIGH.toString()
//                , MStatus.INPROGRESS.toString(), "sum", Date(2020, 10, 20), "ABCDE")
//
//        Mockito.verify(taskRepo, Mockito.times(1)).updateTask("updated", Date(2020, 10, 20), MPriority.HIGH.toString()
//                , MStatus.INPROGRESS.toString(), "sum", Date(2020, 10, 20), "ABCDE")
//    }
//
//    @Test
//    fun deleteTask(){
//        taskRepo.deleteTask("ABCDE")
//        Mockito.verify(taskRepo, Mockito.times(1)).deleteTask("ABCDE")
//    }
//}