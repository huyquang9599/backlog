package com.huy.backlog.model

import java.util.*

data class ProjectDTO(
        var description: String,
        var startDate: Date,
        var endDate: Date,
        var name:String,
        var updatedAt: Date
) {}