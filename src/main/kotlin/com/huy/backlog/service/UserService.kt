package com.huy.backlog.service

import com.huy.backlog.jwt.JwtResponse
import com.huy.backlog.model.User
import com.huy.backlog.model.UserDTO

interface UserService {


    fun getUserByUsername(username: String): User

    fun addUser(user:User)

    fun deleteByUsername(username: String)

    fun authenticateUser(userDTO:UserDTO):JwtResponse
}