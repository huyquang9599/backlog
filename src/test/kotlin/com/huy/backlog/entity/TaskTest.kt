package com.huy.backlog.entity

import com.huy.backlog.model.MPriority
import com.huy.backlog.model.MStatus
import com.huy.backlog.model.Task
import com.huy.backlog.service.TaskService
import org.junit.Assert

import org.junit.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.util.*

@ExtendWith(SpringExtension::class)
@DataJpaTest
class TaskTest{

    @Autowired
    private lateinit var entityManager: TestEntityManager

    @Autowired
    private lateinit var taskRepo: TaskService

    @Test
    fun whenFindByName_thenReturnUser() {
        // given
        val id: Long = Random().nextLong()
        val identifier: String = "${UUID.randomUUID().toString()}-${id}"

        val task = Task(id,identifier,"sum","accCrit",MStatus.INPROGRESS,MPriority.HIGH,Date(2020,10,20),Date(2020,10,20),Date(2020,10,20))
        entityManager.persist(task);
        entityManager.flush();


        // when
        val found: List<Task> = taskRepo.getTask(identifier)


        // then
        Assert.assertEquals(task,found[0])
    }

}