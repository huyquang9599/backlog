package com.huy.backlog.service

import com.huy.backlog.model.Task
import com.huy.backlog.model.TaskDTO
import java.util.*

interface TaskService {
    fun addTask(task: Task,identifier: String)

    fun getTask(identifier:String):List<Task>

    fun deleteTask(taskSequence: String)

    fun updateTaskSequence(newProjectTaskSequence: String, oldProjectTaskSequence: String)

    fun updateTask(taskDTO:TaskDTO)

}