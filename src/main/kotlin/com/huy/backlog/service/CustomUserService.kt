package com.huy.backlog.service


import com.huy.backlog.model.User

import org.springframework.beans.factory.annotation.Autowired

import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class CustomUserService : UserDetailsService {
    @Autowired
    private lateinit var userRepo: UserService

    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(username: String): CustomUserDetails {
        val user: User? = userRepo.getUserByUsername(username)

        if (user == null) {
           throw UsernameNotFoundException("Unknown user:$username")
        }else
           return  CustomUserDetails(user.email,user.username, user.password,"null")
    }
}