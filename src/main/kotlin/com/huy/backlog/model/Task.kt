package com.huy.backlog.model

import lombok.Builder
import java.util.*
import javax.persistence.*
import javax.validation.constraints.Max
import javax.validation.constraints.Min

enum class MStatus {
        TODO, INPROGRESS, DONE
}
enum class MPriority {
        LOW,MEDIUM,HIGH
}


@Entity
@Table(name = "task")
data class Task(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column
        val id: Long,



        @Min(5)
        @Max(5)
        @Column(unique = true,nullable = false )
        var projectTaskSequence: String,

        @Column
        var summary: String,

        @Column
        var acceptanceCriteria: String,

        @Column(nullable = false)
        @Enumerated(EnumType.STRING)
        var status: MStatus,

        @Enumerated(EnumType.STRING)
        @Column(nullable = false)
        var priority:MPriority,

        @Column
        @Temporal(TemporalType.DATE)
        var dueDate: Date,

        @Column
        @Temporal(TemporalType.DATE)
        var createdAt: Date,

        @Column
        @Temporal(TemporalType.DATE)
        var updatedAt: Date

){

        @ManyToOne(fetch = FetchType.EAGER,cascade = [CascadeType.ALL])
        @JoinColumn(name = "backlog")
        var backlog: Backlog ?=null

}