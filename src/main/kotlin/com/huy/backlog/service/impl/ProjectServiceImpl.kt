package com.huy.backlog.service.impl

import com.huy.backlog.model.Backlog
import com.huy.backlog.model.Project
import com.huy.backlog.model.ProjectDTO
import com.huy.backlog.reposity.BacklogRepository
import com.huy.backlog.reposity.ProjectRepository
import com.huy.backlog.service.ProjectService
import org.springframework.stereotype.Service
import java.util.*

@Service
class ProjectServiceImpl(
        private val projectRepo: ProjectRepository,
        private val backlogRepo: BacklogRepository
):ProjectService {
    override fun createProject(project: Project) {
        val backlog = Backlog(null)
        backlog.project=project
        backlogRepo.save(backlog)
        projectRepo.save(project)
    }


    override fun getAllProject(): MutableIterable<Project> {
        return projectRepo.findAll()
    }

    override fun getProjectByIdentifier(identifier: String): Optional<Project> {
      return Optional.of(projectRepo.getProjectByIdentifier(identifier))
    }

    override fun updateProject(projectDTO: ProjectDTO,identifier: String) {
        var project = projectRepo.getProjectByIdentifier(identifier)
        val updateProject = project.copy(
                description = projectDTO.description,
                updatedAt = projectDTO.updatedAt,
                name = projectDTO.name,
                startDate = projectDTO.startDate,
                endDate = projectDTO.endDate
        )
        projectRepo.save(updateProject)
    }


    override fun deleteProject(identifier: String){
        val project= projectRepo.getProjectByIdentifier(identifier)
        projectRepo.delete(project)
    }

}