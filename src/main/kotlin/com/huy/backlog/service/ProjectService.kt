package com.huy.backlog.service

import com.huy.backlog.model.Project
import com.huy.backlog.model.ProjectDTO
import java.util.*

interface ProjectService {
    fun createProject(project: Project)

    fun getAllProject():MutableIterable<Project>

    fun getProjectByIdentifier(identifier: String): Optional<Project>

    fun updateProject(projectDTO: ProjectDTO,identifier: String)

    fun deleteProject(identifier: String)
}