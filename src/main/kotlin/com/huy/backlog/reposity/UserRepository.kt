package com.huy.backlog.reposity

import com.huy.backlog.model.User
import com.huy.backlog.model.UserDTO
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Repository
interface UserRepository : JpaRepository<User, Long> {
  //  @Query(value="""select * from user where username=?1""",nativeQuery = true)
    fun findUserByUsername(username: String): User
}