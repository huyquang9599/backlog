package com.huy.backlog.controller

import org.apache.http.client.ClientProtocolException
import org.apache.http.client.methods.*
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus
import org.springframework.test.context.junit4.SpringRunner
import java.io.IOException


@RunWith(SpringRunner::class)
@SpringBootTest
class TaskControllerTest {

    lateinit var client:CloseableHttpClient

    val taskId:String="1000"

    @Before
    fun setup() {
        client = HttpClients.createDefault()
    }



    @After
    fun done(){
        client.close()
    }

    @Order(1)
    @Test
    @Throws(ClientProtocolException::class, IOException::class)
    fun createTask_then200() {
        val http = HttpPost("http://localhost:8080/api/backlog/ABCDE")
        // When
        val json = """{
                    "backlogId":{"projectIdentifier":"ABCDE"},
                    "projectTaskSequence":"ABCDE",
                    "summary":"a",
                    "acceptanceCriteria":"",
                    "status":"TODO",
                    "priority":"LOW",
                    "dueDate":"2020-10-20",
                    "createdAt":"2020-10-20",
                    "updatedAt":"2020-10-20"
                    }"""
        var entity = StringEntity(json)
        http.entity = entity
        http.setHeader("Accept", "application/json")
        http.setHeader("Content-type", "application/json")
        //http.setHeader(HttpHeaders.AUTHORIZATION, "Bearer $token")


        val response = client.execute(http)

        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),
                response.statusLine.statusCode)

    }

    @Order(2)
    @Test
    @Throws(ClientProtocolException::class, IOException::class)
    fun getTaskByProjectIdentifier_then200() {

        // Given
        val http = HttpGet("http://localhost:8080/api/backlog/ABCDE/ABCDE-$taskId")


        // When


        http.setHeader("Accept", "application/json");
        http.setHeader("Content-type", "application/json");
       // http.setHeader(HttpHeaders.AUTHORIZATION, "Bearer $token")
        val response = client.execute(http)


        // Then
        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),
                response.statusLine.statusCode
        );
    }

    @Order(3)
    @Test
    @Throws(ClientProtocolException::class, IOException::class)
    fun updateTask_then200() {
        // Given
        val http = HttpPatch("http://localhost:8080/api/backlog/ABCDE/ABCDE-$taskId")


        // When
        val json = """{
                    "backlogId":{"projectIdentifier":"ABCDE"},
                    "projectTaskSequence":"ABCDE",
                    "summary":"a",
                    "acceptanceCriteria":"",
                    "status":"DONE",
                    "priority":"HIGH",
                    "dueDate":"2020-10-20",
                    "createdAt":"2020-10-20",
                    "updatedAt":"2020-10-22"
                    }"""
        var entity = StringEntity(json)
        http.entity = entity
        http.setHeader("Accept", "application/json");
        http.setHeader("Content-type", "application/json");
      //  http.setHeader(HttpHeaders.AUTHORIZATION, "Bearer $token")
        val response = client.execute(http)


        // Then
        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),
                response.statusLine.statusCode
        );
    }



    @Order(4)
    @Test
    @Throws(ClientProtocolException::class, IOException::class)
    fun getAllTask_then200() {
        // Given
        val http = HttpGet("http://localhost:8080/api/backlog/ABCDE")


        // When
        http.setHeader("Accept", "application/json");
        http.setHeader("Content-type", "application/json");
        //http.setHeader(HttpHeaders.AUTHORIZATION, "Bearer $token")
        val response = client.execute(http)


        // Then
        Assert.assertEquals(HttpStatus.FORBIDDEN.value(),
                response.statusLine.statusCode
        );

    }




}
