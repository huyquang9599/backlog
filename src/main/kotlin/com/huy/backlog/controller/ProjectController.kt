package com.huy.backlog.controller

import com.huy.backlog.model.Project
import com.huy.backlog.model.ProjectDTO
import com.huy.backlog.reposity.BacklogRepository
import com.huy.backlog.service.ProjectService
import com.huy.backlog.service.TaskService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*


@Controller
@CrossOrigin
@RequestMapping(path = ["/api/projects"])
class ProjectController {
    @Autowired
    lateinit var projectRepo:ProjectService

    @Autowired
    lateinit var backlogRepo: BacklogRepository

    @Autowired
    lateinit var taskRepo: TaskService

    @PostMapping(path = ["/"])
    @ResponseBody
    fun createProject(@RequestBody project: Project): ResponseEntity<*> {
        return ResponseEntity(projectRepo.createProject(project),HttpStatus.CREATED)
    }


    @GetMapping(path = ["/"])
    @ResponseBody
    fun getAllProject(): ResponseEntity<*> {
        return ResponseEntity(projectRepo.getAllProject(),HttpStatus.OK)
    }

    @GetMapping(path = ["/{identifier}"])
    @ResponseBody
    fun getProjectByIdentifier(@PathVariable identifier:String): ResponseEntity<*>{
        return ResponseEntity(projectRepo.getProjectByIdentifier(identifier),HttpStatus.OK)
    }

    @PatchMapping(path = ["/{identifier}"])
    @ResponseBody
    fun updateProject(@RequestBody projectDTO:ProjectDTO, @PathVariable identifier: String): ResponseEntity<*> {
        return ResponseEntity(projectRepo.updateProject(projectDTO,identifier),
                HttpStatus.OK)
    }

    @DeleteMapping(path = ["/{identifier}"])
    @ResponseBody
    fun deleteProject(@PathVariable identifier: String): ResponseEntity<*> {
        return  ResponseEntity.ok(projectRepo.deleteProject(identifier))
    }

}