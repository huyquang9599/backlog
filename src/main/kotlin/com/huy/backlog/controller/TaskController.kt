package com.huy.backlog.controller

import com.huy.backlog.model.Task
import com.huy.backlog.model.TaskDTO
import com.huy.backlog.service.TaskService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*


@Controller
@CrossOrigin
@RequestMapping(path = ["/api/backlog"])
class TaskController {
    @Autowired
    lateinit var taskRepo:TaskService


    @PostMapping(path = ["/{identifier}"])
    @ResponseBody
    fun createTask(@PathVariable identifier: String,@RequestBody task: Task ): ResponseEntity<*>{
        return ResponseEntity(taskRepo.addTask(task,identifier),HttpStatus.CREATED)
    }

    @GetMapping(path = ["/{identifier}"])
    @ResponseBody
    fun getAllTask(@PathVariable identifier: String): ResponseEntity<*> {
        return ResponseEntity(taskRepo.getTask("$identifier-%"),HttpStatus.OK)
    }

    @GetMapping(path = ["/{identifier}/{taskSequence}"])
    @ResponseBody
    fun getTask(@PathVariable identifier: String,@PathVariable taskSequence: String): ResponseEntity<*> {
        return ResponseEntity(taskRepo.getTask(taskSequence),HttpStatus.OK)
    }

    @PatchMapping(path = ["/{identifier}/{taskSequence}"])
    @ResponseBody
    fun updateTask(@PathVariable identifier: String,@PathVariable taskSequence: String, @RequestBody taskDTO:TaskDTO): ResponseEntity<*>? {
       return ResponseEntity(taskRepo.updateTask(taskDTO),HttpStatus.OK )
    }

    @DeleteMapping(path = ["/{identifier}/{taskSequence}"])
    @ResponseBody
    fun deleteTask(@PathVariable identifier: String,@PathVariable taskSequence: String): ResponseEntity<*> {
        return ResponseEntity(taskRepo.deleteTask(taskSequence),HttpStatus.OK)
    }
}