package com.huy.backlog.model

import java.util.*

data class TaskDTO(
        var acceptanceCriteria: String,
        var summary: String,
        var status: MStatus,
        var priority:MPriority,
        var dueDate: Date,
        var projectTaskSequence: String,
        var updatedAt:Date
) {
}