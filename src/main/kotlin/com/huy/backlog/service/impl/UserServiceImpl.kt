package com.huy.backlog.service.impl

import com.huy.backlog.jwt.JwtResponse
import com.huy.backlog.jwt.JwtTokenUtil
import com.huy.backlog.model.User
import com.huy.backlog.model.UserDTO
import com.huy.backlog.reposity.UserRepository

import com.huy.backlog.service.CustomUserDetails
import com.huy.backlog.service.UserService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import java.util.*

@Service
class UserServiceImpl(
        private val userRepo:UserRepository,
        private val authenticationManager: AuthenticationManager,
        private val jwtTokenUtil: JwtTokenUtil

):UserService {
    private val bcryptEncoder = BCryptPasswordEncoder(BCryptPasswordEncoder.BCryptVersion.`$2A`, 31)


    override fun getUserByUsername(username: String): User {
        var userDTO = userRepo.findUserByUsername(username)
        return userDTO!!.copy()

    }

    override fun addUser(user:User) {
        user.password=bcryptEncoder.encode(user.password)
       userRepo.save(user)
    }

    override fun deleteByUsername(username: String){
        var user = userRepo.findUserByUsername(username);
      userRepo.deleteById(user!!.id);
    }

    override fun authenticateUser(userDTO: UserDTO): JwtResponse {
        var authentication: Authentication = authenticationManager.authenticate(UsernamePasswordAuthenticationToken(userDTO.username,userDTO.password))
        SecurityContextHolder.getContext().authentication = authentication
        val token: String = jwtTokenUtil.generateToken(authentication.principal as CustomUserDetails)
        return JwtResponse("ok",token)
    }

}