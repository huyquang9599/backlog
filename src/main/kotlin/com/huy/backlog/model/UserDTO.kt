package com.huy.backlog.model

import javax.persistence.*

data class UserDTO(
        var username: String,
        var password: String
){}