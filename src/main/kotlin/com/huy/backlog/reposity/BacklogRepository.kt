package com.huy.backlog.reposity

import com.huy.backlog.model.*
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*
import javax.transaction.Transactional

@Repository
interface BacklogRepository : CrudRepository<Backlog, Long> {
    @Query(value = """select * from backlog where project_identifier=?1""",nativeQuery = true)
    fun getBacklogByProjectIdentifier(identifier: String):Backlog
}