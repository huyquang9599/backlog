package com.huy.backlog.service.impl

import com.huy.backlog.reposity.BacklogRepository
import com.huy.backlog.reposity.TaskRepository
import com.huy.backlog.service.BacklogService
import org.springframework.stereotype.Service

@Service
class BacklogServiceImpl(private val backlogRepository: BacklogRepository,
                         private val taskRepository: TaskRepository) : BacklogService
 {

}