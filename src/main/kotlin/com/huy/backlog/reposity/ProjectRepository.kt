package com.huy.backlog.reposity

import com.huy.backlog.model.Backlog
import com.huy.backlog.model.Project
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*
import javax.transaction.Transactional
@Repository
interface ProjectRepository : CrudRepository<Project, Long> {

    @Query(value = """select * from project where identifier=?1""",nativeQuery = true)
    fun getProjectByIdentifier(identifier:String):Project
}