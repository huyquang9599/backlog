package com.huy.backlog.reposity
//
//import com.huy.backlog.model.Project
//import com.huy.backlog.model.User
//import org.junit.Assert
//import org.junit.Before
//import org.junit.Test
//import org.junit.runner.RunWith
//import org.mockito.Mockito
//import org.springframework.boot.test.context.SpringBootTest
//import org.springframework.boot.test.mock.mockito.MockBean
//import org.springframework.test.context.junit4.SpringRunner
//import org.springframework.test.web.reactive.server.WebTestClient
//import java.util.*
//
//
//@RunWith(SpringRunner::class)
//@SpringBootTest
//class ProjectRepoTest {
//
//    @MockBean
//    lateinit var projectRepo: ProjectReposity
//
//    var project1=Project(1,"kotlin","ABCDE","project is big",Date(2020,10,20),Date(2020,10,20),Date(2020,10,20),Date(2020,10,20))
//
//    @Test
//    fun findProjectByIdentifier(){
//        Mockito.`when`(projectRepo.getProjectByIdentifier("ABCDE")).thenReturn(Optional.of(project1))
//        var projectTest: Project? = projectRepo.getProjectByIdentifier("ABCDE").get()
//        Assert.assertEquals(project1,projectTest)
//        Mockito.verify(projectRepo, Mockito.times(1)).getProjectByIdentifier("ABCDE")
//    }
//
//    @Test
//    fun countProject(){
//        var list: MutableList<Project> = ArrayList<Project>()
//        var project2=Project(2,"kotlin","ABCDG","project is big",Date(2020,10,20),Date(2020,10,20),Date(2020,10,20),Date(2020,10,20))
//        list.add(project1)
//        list.add(project2)
//
//        Mockito.`when`(projectRepo.findAll()).thenReturn(list)
//        Assert.assertEquals(list.size,projectRepo.findAll().count())
//
//        Mockito.verify(projectRepo, Mockito.times(1)).findAll();
//    }
//
//    @Test
//    fun addProject(){
//        projectRepo.save(project1)
//        Mockito.verify(projectRepo, Mockito.times(1)).save(project1)
//    }
//
//    @Test
//    fun updateProject(){
//        projectRepo.updateProject("kotlinx",Date(2020,10,20),Date(2020,10,20),Date(2020,10,20),"des","ABCDE")
//        Mockito.verify(projectRepo, Mockito.times(1)).updateProject("kotlinx",Date(2020,10,20),Date(2020,10,20),Date(2020,10,20),"des","ABCDE")
//    }
//
//    @Test
//    fun deleteProject(){
//        projectRepo.deleteProject("ABCDE")
//        Mockito.verify(projectRepo, Mockito.times(1)).deleteProject("ABCDE")
//    }
//}
//
//
