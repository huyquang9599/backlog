package com.huy.backlog.model

import com.fasterxml.jackson.annotation.JsonIgnore
import lombok.Builder
import javax.persistence.*

@Entity
@Table(name = "backlog")
data class Backlog(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column
        val id: Long?




) {
        @OneToOne(cascade = [CascadeType.ALL])
        @JoinColumn(name = "projectIdentifier",unique = true)
        var project: Project? = null

        @JsonIgnore
        @OneToMany(mappedBy = "backlog", fetch = FetchType.LAZY)
        var task: MutableList<Task> = mutableListOf()

}