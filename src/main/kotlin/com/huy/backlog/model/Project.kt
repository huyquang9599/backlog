package com.huy.backlog.model


import java.util.*
import javax.persistence.*


@Entity
@Table(name = "project")
data class Project(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column
        val id: Long,

        @Column(nullable = false )
        var name: String,


        @Column(nullable = false,unique = true )
        val identifier: String,

        @Column
        val description: String,

        @Column(nullable = true)
        @Temporal(TemporalType.DATE)
        var startDate: Date,

        @Column(nullable = true)
        @Temporal(TemporalType.DATE)
        var endDate: Date,

        @Column(nullable = true)
        @Temporal(TemporalType.DATE)
        var createdAt: Date,

        @Column(nullable = true)
        @Temporal(TemporalType.DATE)
        var updatedAt: Date
) {

        @OneToOne(mappedBy = "project",cascade = [CascadeType.ALL])
        private val backlog: Backlog? = null

}