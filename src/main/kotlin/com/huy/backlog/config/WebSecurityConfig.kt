package com.huy.backlog.config

import com.huy.backlog.jwt.JwtRequestFilter
import com.huy.backlog.service.CustomUserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder.BCryptVersion
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

@EnableWebSecurity
class WebSecurityConfig : WebSecurityConfigurerAdapter() {
    @Autowired
    var customUserService: CustomUserService? = null

    @Autowired
    private val jwtRequestFilter: JwtRequestFilter? = null

    @Bean // (BeanIds.AUTHENTICATION_MANAGER)
    @Throws(Exception::class)
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }

    @Bean
    fun bCryptEncoder(): BCryptPasswordEncoder {
        return BCryptPasswordEncoder(BCryptVersion.`$2A`, 31)
    }

    @Autowired
    @Throws(Exception::class)
    override fun configure(auth: AuthenticationManagerBuilder) {
        // configure AuthenticationManager so that it knows from where to load
        // user for matching credentials
        // Use BCryptPasswordEncoder
        auth.userDetailsService(customUserService).passwordEncoder(bCryptEncoder())
    }

    @Throws(Exception::class)
    protected override fun configure(http: HttpSecurity) {
        http.headers().frameOptions().sameOrigin()
        http.cors().and().csrf().ignoringAntMatchers("/h2-console/**")
                .disable().authorizeRequests()
                .antMatchers("/api/users/login","/api/users/register","/h2-console/**").permitAll()

                .anyRequest()
                .authenticated().and() // .and().
                // make sure we use stateless session; session won't be used to
                // store user's state.
                .exceptionHandling().and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)

        // Thêm một lớp Filter kiểm tra jwt
        http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter::class.java)
    }
}