package com.huy.backlog.controller

import com.huy.backlog.jwt.JwtResponse
import com.huy.backlog.jwt.JwtTokenUtil
import com.huy.backlog.model.User
import com.huy.backlog.model.UserDTO


import com.huy.backlog.service.CustomUserDetails
import com.huy.backlog.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder.BCryptVersion
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.validation.Valid


@Controller
@CrossOrigin
@RequestMapping(path = ["/api/users"])
class UserController {
    @Autowired
    lateinit var userRepo:UserService
    @PostMapping(path = ["/login"])
    @ResponseBody
    fun login(@RequestBody userDTO:UserDTO): ResponseEntity<*> {
        return ResponseEntity.ok(userRepo.authenticateUser(userDTO))
    }

    @PostMapping(path = ["/register"])
    @ResponseBody
    fun register(@Valid @RequestBody user:User): ResponseEntity<*> {
        return ResponseEntity(userRepo.addUser(user),HttpStatus.CREATED)
    }

}