package com.huy.backlog.service.impl

import com.huy.backlog.model.Task
import com.huy.backlog.model.TaskDTO
import com.huy.backlog.reposity.BacklogRepository
import com.huy.backlog.reposity.TaskRepository
import com.huy.backlog.service.TaskService
import org.springframework.stereotype.Service

@Service
class TaskServiceImpl(private val taskRepo:TaskRepository,
                      private val backlogRepo:BacklogRepository):TaskService {
    override fun addTask(task: Task,identifier: String){
        var backlog = backlogRepo.getBacklogByProjectIdentifier(identifier)
        task.backlog=backlog
        var taskn = taskRepo.save(task)
        taskn.projectTaskSequence+="-${taskn.id}"
        taskRepo.save(taskn)
    }

    override fun getTask(identifier: String): List<Task> {
        return taskRepo.getTask(identifier)
    }

    override fun deleteTask(taskSequence: String) {
        var task = taskRepo.getTask(taskSequence)[0]
        taskRepo.delete(task)
    }

    override fun updateTaskSequence(newProjectTaskSequence: String, oldProjectTaskSequence: String){
        taskRepo.updateTaskSequence(newProjectTaskSequence,oldProjectTaskSequence)
    }

    override fun updateTask(taskDTO: TaskDTO) {
        var task = taskRepo.getTask(taskDTO.projectTaskSequence)[0]
        val updateTask = task.copy(
                summary = taskDTO.summary,
                updatedAt = taskDTO.updatedAt,
                dueDate = taskDTO.dueDate,
                priority = taskDTO.priority,
                status = taskDTO.status,
                acceptanceCriteria = taskDTO.acceptanceCriteria
        )
       taskRepo.save(updateTask)
    }
}