package com.huy.backlog.controller


import com.huy.backlog.reposity.UserRepository
import org.apache.http.client.ClientProtocolException
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus
import org.springframework.test.context.junit4.SpringRunner
import java.io.IOException


@RunWith(SpringRunner::class)
@SpringBootTest
class UserControllerTest{

    @Autowired
    lateinit var userRepo: UserRepository

    lateinit var client:CloseableHttpClient

    @Before
    fun setup(){
        client = HttpClients.createDefault()
    }

    @After
    fun done(){
        client.close()
    }


    @Test
    @Throws(ClientProtocolException::class, IOException::class)
    fun registUser_notStrongPassword_then406() {

        // Given
        val http = HttpPost("http://localhost:8080/api/users/register")


        // When
        val json= """{"username":"huy2","password":"Abcdefg","email":"user@gmail.com"}""";
        var entity = StringEntity(json);
        http.entity = entity;
        http.setHeader("Accept", "application/json");
        http.setHeader("Content-type", "application/json");

        val response = client.execute(http)


        // Then
        Assert.assertEquals(HttpStatus.NOT_ACCEPTABLE.value(),
                response.statusLine.statusCode
        );
    }
    @Order(1)
    @Test
    @Throws(ClientProtocolException::class, IOException::class)
    fun registUser_ok_then200() {

        // Given
        val http = HttpPost("http://localhost:8080/api/users/register")

        userRepo.deleteById(1)
        // When
        val json= """{"username":"huy2","password":"Abcdefg0~","email":"user@gmail.com"}""";
        var entity = StringEntity(json);
        http.entity = entity;
        http.setHeader("Accept", "application/json");
        http.setHeader("Content-type", "application/json");

        val response = client.execute(http)

        // Then
        Assert.assertEquals(HttpStatus.CREATED.value(),
                response.statusLine.statusCode
        );
    }
//    @Order(2)
//    @Test
//    @Throws(ClientProtocolException::class, IOException::class)
//    fun registUser_duplicateValue_then406() {
//
//        // Given
//        val http = HttpPost("http://localhost:8080/api/users/register")
//
//
//        // When
//        val json= """{"username":"huy2","password":"Abcdefg0~","email":"user@gmail.com"}""";
//        var entity = StringEntity(json);
//        http.entity = entity;
//        http.setHeader("Accept", "application/json");
//        http.setHeader("Content-type", "application/json");
//
//        val response = client.execute(http)
//
//
//        // Then
//        Assert.assertEquals(HttpStatus.NOT_ACCEPTABLE.value(),
//                response.statusLine.statusCode
//        );
//
//    }


    @Test
    @Throws(ClientProtocolException::class, IOException::class)
    fun rightCredentials_then200() {

        // Given

        val http = HttpPost("http://localhost:8080/api/users/login")


        // When
        val json= """{"username":"huy2","password":"Abcdefg0~"}""";
        var entity = StringEntity(json);
        http.entity = entity;
        http.setHeader("Accept", "application/json");
        http.setHeader("Content-type", "application/json");

        val response = client.execute(http)

        // Then
        Assert.assertEquals(HttpStatus.OK.value(),
                response.statusLine.statusCode
        );

    }

    @Test
    @Throws(ClientProtocolException::class, IOException::class)
    fun wrongCredentials_then406() {

        // Given
        val http = HttpPost("http://localhost:8080/api/users/login")


        // When
        val json= """{"username":"huy2","password":"Abcdefg0"}""";
        var entity = StringEntity(json);
        http.entity = entity;
        http.setHeader("Accept", "application/json");
        http.setHeader("Content-type", "application/json");

        val response = client.execute(http)


        // Then
        Assert.assertEquals(HttpStatus.NOT_ACCEPTABLE.value(),
                response.statusLine.statusCode
        );

    }


}
