package com.huy.backlog.reposity

import com.huy.backlog.model.*
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.*
import javax.transaction.Transactional
@Repository
interface TaskRepository : CrudRepository<Task, Long> {
    @Query(value = """select * from task where project_task_sequence like "?1" """,nativeQuery = true)
    fun getTask(identifier:String):List<Task>

    @Modifying
    @Transactional
    @Query(value = """UPDATE task
                        SET project_task_sequence=?1
                        WHERE project_task_sequence = ?2 """,nativeQuery = true)
    fun updateTaskSequence(newProjectTaskSequence: String, oldProjectTaskSequence: String)
}