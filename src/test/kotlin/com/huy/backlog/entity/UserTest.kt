package com.huy.backlog.entity

import com.huy.backlog.model.User
import com.huy.backlog.reposity.UserRepository
import com.huy.backlog.service.UserService

import org.junit.Assert
import org.junit.Before

import org.junit.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.context.junit4.SpringRunner
import java.util.*
import javax.persistence.EntityManager

@RunWith(SpringRunner::class)
@DataJpaTest
class UserTest{
    @Autowired
    private lateinit var  entityManager: EntityManager

    @Autowired
    private var userRepo: UserService?=null



    @Test
    fun whenFindByName_thenReturnUser() {
        // given
        val id: Long = Random().nextLong()
        val name="huy"// UUID.randomUUID().toString()
        val user = User(1,name,"mail@gmail.com","123")
       entityManager.persist(user)
        entityManager.flush()

        // when
        val found: User? = userRepo?.getUserByUsername(name)


//         then
        Assert.assertEquals(user,found.toString())
    }

}