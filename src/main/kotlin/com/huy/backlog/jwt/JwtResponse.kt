package com.huy.backlog.jwt

import java.io.Serializable

data class JwtResponse(val response:String, val token: String) : Serializable {

    companion object {
        private const val serialVersionUID = -8091879091924046844L
    }

}