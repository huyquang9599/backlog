package com.huy.backlog

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BacklogApplication

fun main(args: Array<String>) {
	runApplication<BacklogApplication>(*args)
}
