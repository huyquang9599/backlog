package com.huy.backlog.model

import com.fasterxml.jackson.annotation.JsonIgnore
import lombok.Builder
import org.hibernate.validator.constraints.Length
import javax.persistence.*
import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Pattern

@Entity
@Table(name = "user")
data class User(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    val id: Long,

    @Column(nullable = false,unique = true )
    var username: String,

    @get:Email(message = "email invalid")
    @Column(unique = true )
    var email: String,

    @get:Pattern(regexp="^(?=\\D*\\d)(?=[^a-z]*[a-z])(?=[^A-Z]*[A-Z]).{8,30}\$",
            message="Password is not strong enough")
    @Column(nullable = false )
    var password: String



){}